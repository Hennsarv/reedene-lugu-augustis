﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spordipäev
{
    static class Program
    {
        static string Join(this IEnumerable<string> osad, string sep)
            => string.Join(sep, osad);

        static TimeSpan ToTimeSpan(this string text) => TimeSpan.Parse(text);

        static void Main(string[] args)
        {
            string filename = @"..\..\spordipäeva protokoll.txt";
            var protokoll =
            System.IO.File.ReadAllLines(filename)
                .Skip(1)
                .Where(x => x.Trim() != "")
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
                .Select(x => new
                {
                    Nimi = x[0],
                    Distants = int.Parse(x[1]),
                    Aeg =
                    ("0:0:" + x[2])
                                .Split(':')
                                .Reverse()
                                .Take(3)
                                .Reverse()
                                .Join(":").ToTimeSpan().TotalSeconds
                                ,

                })
                    .Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants / x.Aeg })
                  .ToList()
                  ;
            Console.WriteLine("\nprotokoll:");
            protokoll.ForEach(x => Console.WriteLine(x));
            Console.WriteLine("\nkiireim:");
            Console.WriteLine(
                protokoll.OrderByDescending(x => x.Kiirus).Take(1).FirstOrDefault()
            );

            Console.WriteLine("\niga distantsi kiireim");
            protokoll.ToLookup(x => x.Distants)
                .Select(x => new
                {
                    Distants = x.Key,
                    Kiireim = x.OrderByDescending(y => y.Kiirus).Take(1).Single().Nimi
                })
                .ToList()
                .ForEach(x => Console.WriteLine(x));
        }
    }
}
