﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableTüüp
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 7;
            Nullable<int> b = a;
            int? c = a;
            DateTime? sünniaeg = null;
            Console.WriteLine(sünniaeg.HasValue);

            int d = c ?? 77777777;
            d = c.HasValue ? c.Value : 77777777;

            Koer k = new Koer("Pauka");
            //k = null;
            Console.WriteLine(k?.Nimi ?? "koera enam pole");


        }
    }

    class Koer
    {
        public string Nimi;
        public Koer(string nimi) => Nimi = nimi;

        public override string ToString()
            => $"Koer nimega {Nimi}";

        public string TeeHäält() => $"{Nimi} haugub";
    }
}
