﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelPisutFunktsioonidest
{
    static class E
    {
        public static int JagameKahega(this int x) => x / 2;

        public static IEnumerable<int> Paaris(this IEnumerable<int> arvud)
        {
            foreach (var x in arvud) if (x % 2 == 0) yield return x;
        }

        public static IEnumerable<int> Millised(this IEnumerable<int> arvud, Func<int,bool> f)
        {
            foreach (var x in arvud) if (f(x)) yield return x;
        }

        public static IEnumerable<T2> 
            MinuSelect<T, T2>(this IEnumerable<T> asjad, Func<T, T2> f)
        {
            foreach (var x in asjad) yield return f(x);
        }

        public static string Join(this IEnumerable<string> osad, string sep = " ")
            => String.Join(sep, osad);



    }

    class Program
    {
        static void Main(string[] args)
        {
            // teeme midagi lihtsat
            // teeme funktsiooni, millele antakse ette kaks arvu 
            // ja ta vahetab need ära

            int x = 7; int y = 10;
            Console.WriteLine($"x={x} y={y}");
            Vaheta(ref x, ref y);
            Console.WriteLine($"x={x} y={y}");
            string nimi = "Henn";
            string teinenimi = "Sarvik";
            Console.WriteLine($"{nimi} {teinenimi}");
            Vaheta<string>(ref nimi, ref teinenimi);
            Console.WriteLine($"{nimi} {teinenimi}");

            Func<int, int> ohoo = Kolmega;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday) ohoo = Viiega;
            Console.WriteLine(ohoo(17));

            ohoo = xxx => xxx / 6;
            Console.WriteLine(ohoo(17));

            Console.WriteLine();
            int[] natuke = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var summa = natuke.Where(i => i % 2 == 0).Sum();

            foreach (var v in natuke.Paaris()) Console.WriteLine(v);

            foreach (var v in natuke
                .Select(i => i * i)
                ) Console.WriteLine(v);

            string[] miskidasjad = { "Henn", "50", "Ants", "36", "100", "Kõik" };

            foreach (var v in natuke
                .Where(i => i > 5)
                .Where(i => i % 2 == 0)
                ) Console.WriteLine(v);

            string[] nimed = { "Henn", "Ants", "Peeter", "Joosep" };
            foreach (var v in nimed
                //.Where(i => i.Length > 4)
                .OrderByDescending(i => i)
                ) Console.WriteLine(v);

            int uussumma = miskidasjad
                .Where(i => int.TryParse(i, out int _))
                .Select(i => int.Parse(i))
                .Sum();
            Console.WriteLine(uussumma);

            uussumma = miskidasjad
                .Select(i => { int _ = 0; return int.TryParse(i, out _) ? _ : 0; } )
                .Sum();

            Console.WriteLine(natuke
                .Select(n => n.ToString()).Join("+")
                + "=" + natuke.Sum().ToString()
                );

        }

        static void Vaheta<T>(ref T a, ref T b)
        {
            T ajutine = a;
            a = b;
            b = ajutine;
        }

        static int Kolmega(int mida)
        {
            return mida / 3;
        }

        static int Viiega(int mida)
        {
            return mida / 5;
        }

      



    }

    class Test
    {
        // meenutame, mis on staatiline ja mis objekti meetod
        // Test.TeemeMidagi( ... )
        public static void TeeMidagi(Test x)
        {

        }

        // muutuja.TeemeMidagi()
        public void Teemidagi()
        {
            TeeMidagi(this);
        }

    }
}
